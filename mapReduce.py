from mrjob.job import MRJob
from mrjob.protocol import RawValueProtocol
import parser
import json
from DbInterface import DbInterface
import ast
import pageRank
import anchor

db = DbInterface()
TOTAL_NUM_DOC = 33984

class MRWordCount(MRJob):
    INPUT_PROTOCOL = RawValueProtocol
    OUTPUT_PROTOCOL = RawValueProtocol

    def mapper(self, _ , line):
        key_val_pair = str(line).split('\t')
        if (len(key_val_pair) != 2):
            return

        rawdata_loc = key_val_pair[0]
        url = key_val_pair[1]
        if not parser.is_valid(url):
            return
        fs = open("/Users/sbr/Downloads/WEBPAGES_RAW/" + rawdata_loc)
        #fs = open("./WEBPAGES_RAW/" + rawdata_loc)
        rawdata = fs.read()
        docid = parser.encodeDocID(rawdata_loc)
        tokens = parser.parseWebPage(rawdata, docid, url, db)
        postings = parser.getPosting(docid, tokens)
        for key in postings:
            yield key,json.dumps(postings[key])

    def reducer(self, term, postings):
        new_postings = []
        for entry in list(postings):
            new_postings.append(ast.literal_eval(entry))
        entry ={'term':term, 'postings':new_postings, 'IDF-weight' : parser.calculateIDF(len(new_postings),TOTAL_NUM_DOC)}
        db.insert("test2",entry)

f = open('./WEBPAGES_RAW/bookkeeping.json')
data = json.load(f)
dict = {}
for key, value in data.iteritems():
    dict[value] = key


if __name__ == '__main__':
    import time
    start = int(round(time.time()))
    #insert docID to URL mapping

    dict = []
    for rawdata_loc, url in data.iteritems():
        dict.append({'docID': parser.encodeDocID(rawdata_loc), 'url': url})
    db.insertMany("docIndex", dict)

    MRWordCount.run()
    db.insertMany('collAnchorInfo', anchor.anchorInfo)
    db.insertMany('collAnchorInfo2gram', anchor.anchorInfo_2gram)
    db.insertMany('collTitle', anchor.titleContent)
    db.insertMany('collTitle2gram', anchor.titleContent2gram)
    pageRank.computePageRank()
    end = int(round(time.time()))
    print "time taken = ", end - start