from DbInterface import DbInterface
import parser
import json
db = DbInterface()

f = open('./WEBPAGES_RAW/bookkeeping.json')
data = json.load(f)
dict = []
for rawdata_loc, url in data.iteritems():
    dict.append({'docID' : parser.encodeDocID(rawdata_loc) , 'url' : url})
db.insertMany("docIndex",dict)
