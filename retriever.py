import operator
import itertools
import re
import TfIDFScore
import pageRank
import parser
import proximityScore
from collections import Counter
from DbInterface import DbInterface
from bs4 import BeautifulSoup as bs
import anchor
import bs4
db = DbInterface()
N = 10
#weights = {'tfidf': 1.0, 'pagerank':10 * 100000, 'anchor' : 2.5 * 1000 , 'title':4*1000, 'pos': 2.0}
weights = {'tfidf': 1.0, 'pagerank':0, 'anchor' : 0 , 'title':0, 'pos': 0}

def processQuery(query):
    queryWords = parser.tokenize(str(query))

    tfIdfdocs = TfIDFScore.getTfIDFScore(queryWords)
    anchor_score , title_score = anchor.extractAnchorScore(queryWords,db)
    doc_ids = set(anchor_score.keys() + tfIdfdocs.keys())
    pagerank_score = pageRank.getPageRankScores(doc_ids)
    proximity_score = proximityScore.getProximityScores(queryWords,db)
    print proximity_score

    champion_list1,champion_list2,champion_list3 = combine_scores(tfIdfdocs,anchor_score,title_score,pagerank_score,proximity_score)
    result = champion_list1
    result.update(champion_list2)
    result.update(champion_list3)
    sortedDict = getTopN(result,N)
    result = formatOutput(sortedDict,query)
    return result

def compute_weighted_sum(docId,tfidf_dict,anchor_score,title_score,pagerank_score,proximity_score):
    score = 0
    if anchor_score.has_key(docId):
        score += anchor_score[docId] * weights['anchor']
    if title_score.has_key(docId):
        score +=  title_score[docId] * weights['title']
    if tfidf_dict.has_key(docId):
        score += tfidf_dict[docId] * weights['tfidf']
    if pagerank_score.has_key(docId):
        score += weights['pagerank'] * pagerank_score[docId]
    if proximity_score.has_key(docId):
        score += weights['pos'] * proximity_score[docId]

    return score

def combine_scores(tfidf_dict, anchor_score, title_score,pagerank_score,proximity_score):
    champion_list1 = {}
    champion_list2 = {}
    champion_list3 = {}
    set1 = set(anchor_score.keys()).intersection(title_score.keys())
    for docID in set1:
        champion_list1[docID] = compute_weighted_sum(docID,tfidf_dict,anchor_score,title_score,pagerank_score,proximity_score)

    if len(champion_list1) > N:
        return champion_list1,champion_list2,champion_list3

    anchor_docids = set(anchor_score.keys())
    title_docids = set(title_score.keys())
    set2 = anchor_docids.union(title_docids).difference(set1)
    for docID in set2:
        champion_list2[docID] = compute_weighted_sum(docID,tfidf_dict,anchor_score,title_score,pagerank_score,proximity_score)

    if len(champion_list1) + len(champion_list2) > N:
        return champion_list1,champion_list2,champion_list3

    set3 = set(tfidf_dict.keys())
    set3=set3.difference(anchor_score.keys())
    for docID in set3:
        champion_list3[docID] = compute_weighted_sum(docID,tfidf_dict,anchor_score,title_score,pagerank_score,proximity_score)


    return champion_list1,champion_list2,champion_list3


def getTopN(results,N):
    # http://stackoverflow.com/questions/11902665/top-values-from-dictionary
    N = min(N,len(results))
    sortedDict = Counter(results)
    return sortedDict.most_common(N) #return val is a list of tuple : (docID,score)

def formatOutput(results,query):
    output = []
    query = re.sub("[^\w]", " ", query).lower().split()
    for docID,score in results:
        url = getUrl(docID)
        raw_data_loc = "./WEBPAGES_RAW/" + docID
        raw_data_file = open(raw_data_loc)
        raw_data = raw_data_file.read()
        page = bs(raw_data, 'html.parser')
        title = page.title.string if page.title is not None and page.title.string is not None else url

        texts = page.find_all(text=True)
        word_list = [parser.tokenize_without_stemming(str(element)) for element in texts \
                     if not element.parent.name in ['style', 'script', '[document]'] \
                     and not isinstance(element, bs4.element.Comment)]
        word_list = filter(None, word_list)
        word_list = list(itertools.chain(*word_list))
        word_list.extend(title.split(' '))
        word_list_2 = [parser.stemmer.stem(word) for word in word_list]
        #word_list_2 = [word for word in word_list]
        missing_words = [word for word in query if word not in word_list and word not in word_list_2]
        tmp_index = 0
        for word in query:
            if word in word_list:
                tmp_index = word_list.index(word)
        lower_bound = 0
        if tmp_index > 9:
            lower_bound = tmp_index - 8
        summary = ' '.join(word_list[lower_bound: tmp_index + 8])
        bold_word = [word for word in query if word in summary]
        for word in bold_word:
            summary = summary.replace(word, '<b>' + word + '</b>')
        try:
            summary = unicode(summary, errors='replace')
        except:
            summary = summary
        output.append({'link': url, \
                       'title': title, \
                       'desc': summary, \
                       'missing': ', '.join(missing_words) if len(missing_words) else None, \
                       'bold': bold_word
                       })

    return output

def getUrl(docID):
    obj = db.getUrl("docIndex",docID)
    # print docID
    return obj['url']