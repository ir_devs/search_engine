import glob
import csv

import re
from bs4 import BeautifulSoup as bs
import urlparse
import urllib
import parser
import redis
import sys
from pageRank import computePageRank
from DbInterface import DbInterface

DOCID_URL = 1
URL_DOCID = 2

DB_ANCHOR_TEXT = 'a'
DB_ANCHOR_SOURCE = 's'
DB_ANCHOR_TARGET = 't'

alllinks = set()
def create_docids_url_db():
    count = 0
    with open('WEBPAGES_RAW/bookkeeping.tsv', 'r') as fs:
            tsvreader = csv.reader(fs, delimiter="\t")
            for line in tsvreader:
                key_val_pair = str(line).split('\t')
                if (len(key_val_pair) != 2):
                    return
                url = key_val_pair[1]
                rawdata_loc = key_val_pair[0]
                fs = open("/Users/pratikshetty/PycharmProjects/search_engine/WEBPAGES_RAW/" + rawdata_loc)
                # / Users / pratikshetty / PycharmProjects / search_engine / WEBPAGES_RAW/
                rawdata = fs.read()
                try:
                    outlinks = extract_links(html=rawdata, url=url)
                    global all_links
                    all_links.add(url)
                    all_links = all_links.union(outlinks)
                    count+=1
                    if(count%1000 == 0):
                        print count
                except:
                    print url

            rdb0 = redis.StrictRedis(db=DOCID_URL)
            rdb1 = redis.StrictRedis(db=URL_DOCID)
            docId = 0
            for link in all_links:
                rdb0.set(docId, link)
                rdb1.set(link, docId)
                docId += 1
            if docId%1000 == 0:
                print docId


def extract_links(html='', url=''):
    '''get outgoing links from page'''
    page = bs(html, 'html.parser')
    anchors = page.find_all('a')
    links = [urlparse.urljoin(url, str(urllib.unquote(['href']))).replace('https://', 'http://').strip('/')\
             for anchor in anchors \
             if anchor.has_attr('href') \
             and not anchor['href'].startswith('mailto') \
             and not anchor['href'].startswith('javascript')]
    links = filter(is_url_valid, links)
    return set(links)

def is_url_valid(url):
    '''
    check if URL is a valid url
    '''
    url_struct = urlparse.urlparse(url)
    bad_urls = []
    bad_urls.append('/datasets/datasets' in url)
    bad_urls.append('respond#respond' in url)
    bad_urls.append(re.match('.*\?(.+=.+)&(.+=.+)&.*', url) is not None)
    bad_urls.append(len(url) > 150)
    bad_urls.append(len(url.split('/')) > 10)
    bad_urls.append(url_struct.scheme not in ['http','https',''])
    bad_urls.append('action=login' in url)
    bad_urls.append('ics.uci.edu' not in url)
    pattern = '.*\.(css|js|bmp|gif|jpe?g|ico|png|tiff?|mid|mp2|mp3|mp4' \
                + '|wav|avi|mov|mpeg|ram|m4v|mkv|ogg|ogv|pdf' \
                + '|ps|eps|tex|ppt|pptx|doc|docx|xls|xlsx|names|data|dat|exe|bz2' \
                + '|tar|msi|bin|7z|psd|dmg|iso|epub|dll|cnf|tgz|sha1|xaml|pict' \
                + '|thmx|mso|arff|rtf|jar|csv|java|javac|cc|h|cpp|py|pyc|m|c|db|md5|nb|lif|xml|r|networks|odc' \
                + '|rm|smil|wmv|swf|wma|zip|rar|gz|lzip|lha|arj|wmz|pcz|lsp|pov|z|bib|rkt' \
                + '|3gp|amr|au|vox|rar|aac|ace|alz|apk|arc|txt|wcz|svm|ss|svg|Z|ma)$'
    bad_urls.append(re.match(pattern, url_struct.path.lower()))
    return not any(bad_urls)


def create_webgraph(doc_id, html='', url=''):
    anchortext_links = []
    anchortext_info = []
    try:
        db = DbInterface()
        rdb1 = redis.StrictRedis(db=URL_DOCID)
        page = bs(html, 'html.parser')
        anchors = page.find_all('a')
        if not urlparse.urlparse(url).scheme:
            url = "http://" + url
        anchortext_links = [[str(anchor.text).strip(),
                             urlparse.urljoin(url, str(urllib.unquote(anchor['href']))).strip('/')] \
                            for anchor in anchors \
                            if anchor.has_attr('href') \
                            and not anchor['href'].startswith('mailto') \
                            and not anchor['href'].startswith('javascript')]
        for text, outlink in anchortext_links:
            if text != '' and parser.is_valid(outlink):
                outlink = outlink.replace("http://","")
                outlink = outlink.replace("https://", "")
                anchortext_info.append({DB_ANCHOR_TEXT: text.strip(), DB_ANCHOR_TARGET: rdb1.get(outlink), DB_ANCHOR_SOURCE: doc_id})
    except:
        print anchortext_links
        print(sys.exc_info()[0])
        print
        return

    if len(anchortext_info) > 0:
        db.insertMany("webgraph", anchortext_info)


# def create_dbs():
#     rdb1 = redis.StrictRedis(db=URL_DOCID)
#     with open('WEBPAGES_RAW/bookkeeping.tsv', 'r') as fs:
#             tsvreader = csv.reader(fs, delimiter="\t")
#             for line in tsvreader:
#                 key_val_pair = str(line).split('\t')
#                 if (len(key_val_pair) != 2):
#                     return
#                 url = key_val_pair[1]
#                 rawdata_loc = key_val_pair[0]
#                 fs = open("/Users/pratikshetty/PycharmProjects/search_engine/WEBPAGES_RAW/" + rawdata_loc)
#                 # / Users / pratikshetty / PycharmProjects / search_engine / WEBPAGES_RAW/
#                 rawdata = fs.read()
#                 doc_id = rdb1.get(url)
#                 tokens = parser.parseWebPage(rawdata)
#                 postings = parser.getPosting(doc_id, tokens)
#                 create_webgraph(doc_id=doc_id, html=rawdata, url=url)
#create_docids_url_db()
#create_dbs()
#computePageRank()