import math


def getProximityScores(queryWords,db):
    if(len(queryWords) < 2):
        return {}
    docList = {}
    for word in queryWords:
        cursor = db.getEntry("test2",word)
        if not cursor:
            continue
        for entry in cursor['postings']:
            docID = entry['DB_TFIDF_DOCID']
            if docID in docList:
                docList[docID][word] = entry['DB_TFIDF_POSITIONS']
            else:
                docList[docID] = {word:entry['DB_TFIDF_POSITIONS']}

    # Compute position score for each document
    doc_scores = {}
    for docID,termPositions in docList.items():
        if(len(termPositions) >= 2):
            minDistance = computeMinDistance(termPositions)
            if docID not in doc_scores:
                doc_scores[docID] = positionScore(minDistance)
    return doc_scores

def computeMinDistance(termPositions):
    if(len(termPositions) < 2):
        return 0

    merged_positions = []
    # Merge position lists
    for term,positionList in termPositions.items():
        for position in positionList:
            merged_positions.append((term,position))

    def getKey(item):
        return item[1]
    merged_positions = sorted(merged_positions,key=getKey)

    # Compute min distance
    possible_values = [(merged_positions[i+1][1]-merged_positions[i][1] - 1) for i in xrange(len(merged_positions) - 1) if merged_positions[i][0] != merged_positions[i+1][0]]
    return min(possible_values)

def positionScore(distance):
    alpha = 0.3
    return math.log1p(alpha+math.exp(-1.0*distance))