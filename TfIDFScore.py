import retriever
'''from DbInterface import DbInterface
db = DbInterface()'''

def getTfIDFScore(query):
    docList = {}
    for word in query:
        cursor = retriever.db.getEntry("test2",word)
        '''cursor = db.getEntry("test2", word)'''
        if not cursor:
            continue
        for entry in cursor['postings']:
            score = entry['DB_TFIDF_SCORE'] * cursor['IDF-weight']
            docID = entry['DB_TFIDF_DOCID']
            if docID is None:
                print
                print word
                print '########'
                continue
            if docID in docList:
                docList[docID] += score
            else:
                docList[docID] = score
    m1, m2 = min(docList.values()), max(docList.values())
    if m2 - m1 > 0:
        for d in docList:
            docList[d] /= (m2)
    '''
    for doc_id, score in docList.iteritems():
        docList[doc_id] = 1.0/(1+math.exp(-score))
    '''
    return docList