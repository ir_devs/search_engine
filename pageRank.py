from DbInterface import DbInterface
import redis
import networkx as nx

DOCID_PAGERANK = 3


db = DbInterface()
def constructGraph():
    G = nx.DiGraph()
    cursor = db.getEntries("collAnchorInfo")
    edgeList = [(doc['DB_ANCHOR_TEXT_SOURCE_DOCID'],doc['DB_ANCHOR_TEXT_DEST_DOCID']) for doc in cursor if doc['DB_ANCHOR_TEXT_SOURCE_DOCID']]
    G.add_edges_from(edgeList)
    return G

def computePageRank():
    G = constructGraph()
    results = nx.pagerank(G)
    rdb3 = redis.StrictRedis(db=DOCID_PAGERANK)
    for doc_id, pagerank in results.iteritems():
        rdb3.set(doc_id, pagerank)
    return results

def getPageRankScores(doc_ids):
    rdb3 = redis.StrictRedis(db=DOCID_PAGERANK)
    pagerank_scores = {}
    for doc_id in doc_ids:
        score = rdb3.get(doc_id)
        pagerank_scores[doc_id] = float(score) if score is not None else 0
    return pagerank_scores
