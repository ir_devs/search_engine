import pymongo
from pymongo import MongoClient
import bson
import math

'''
INVERTED_INDEX_DB = "IR-database"
INVERTED_INDEX_COLLECTION = "IR-Collection"

client = MongoClient()
db = client[INVERTED_INDEX_DB]
collection = db[INVERTED_INDEX_COLLECTION]
'''

class DbInterface:
    dbConn = None
    client = None

    def __init__(self):
        try:
            self.client = MongoClient()
        except pymongo.errors.ConnectionFailure, e:
            print "Could not connect to MongoDB: %s" % e
        self.dbConn = self.client['test']

    def insert(self,collName,dictionary):
        self.dbConn[collName].insert(dictionary)

    def insertMany(self,collName,dictionary):
        self.dbConn[collName].insert_many(dictionary)

    def getUrl(self,collName,docID):
        return self.dbConn[collName].find_one({'docID': docID})

    def getDocID(self, collName, url):
        return self.dbConn[collName].find_one({'url': url})

    def getAnchorTargetDocID(self, collName, query):
        return self.dbConn[collName].find({'DB_ANCHOR_TEXT': query})

    def getTitleTargetDocID(self, collName, query):
        return self.dbConn[collName].find({'DB_TITLE_NAME': query})

    def insertIfNotPresent(self, collName, dict):
        for entry in dict:
            result = self.dbConn[collName].find({'DB_TFIDF_TERM': entry['DB_TFIDF_TERM']}).count()
            print "Result" + str(result)
            if result == 0:
                self.dbConn[collName].insert(entry)
            else:
                self.dbConn[collName].update(
                    {'DB_TFIDF_TERM': entry['DB_TFIDF_TERM']},
                    {'$push' : {'DB_TFIDF_POSTING': entry['DB_TFIDF_POSTING'][0]}}
                )
                self.dbConn[collName].update(
                    {'DB_TFIDF_TERM': entry['DB_TFIDF_TERM']},
                    {'$inc': {'DB_TFIDF_POSTING_SIZE': 1}}
                )

    def idf(self, collName, totalDocs):
        for entry in self.dbConn[collName].find():
            print entry
            entry['DB_TFIDF_IDF'] = math.log10((1.0 * totalDocs)/(entry['DB_TFIDF_POSTING_SIZE']))
            self.dbConn[collName].save(entry)

    def getEntry(self, collName, term):
        return self.dbConn[collName].find_one({'term': term})

    def getEntries(self, collName):
        return self.dbConn[collName].find()