import glob
import string
import urllib
import json
import re
from urlparse import urlparse
from nltk.stem import SnowballStemmer
stemmer = SnowballStemmer("english")
import bs4
from bs4 import BeautifulSoup as bs
import nltk
import math
from nltk.tokenize import RegexpTokenizer
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import anchor

#https://github.com/karthikprasad/krisp-search

num_Documents = 0
stopwords = nltk.corpus.stopwords.words('english')

def getPosting(docid, tokens):
    docSize = len(tokens)
    tokenMap = positionMap(tokens)
    termPositions = {}
    position_rows = {}
    for key, value in tokenMap.iteritems():
        position_rows[key] = {'DB_TFIDF_DOCID': docid, 'DB_TFIDF_POSITIONS': value,
                        'DB_TFIDF_SCORE': termFrequencyWt(len(value))}
    return position_rows

def calculateIDF(termDocs, totalDocs):
    if(termDocs == 0):
        return 0
    result = math.log10((1.0 * totalDocs)/(termDocs))
    return result

def termFrequencyWt(freq):
    if(freq == 0):
        return 0
    result = 1.0 + math.log10(1.0 * freq)
    return result

def positionMap(tokens):
    tokenMap = {}
    for i in xrange(len(tokens)):
        if tokens[i] in tokenMap:
            tokenMap[tokens[i]].append(i)
        else:
            tokenMap[tokens[i]] = [i]
    return tokenMap

def encodeDocID(rawdata_loc):
    #to-do uncomment for final submission
    # folder,file = rawdata_loc.split("/")
    # return int(folder)*500+int(file)
    return rawdata_loc

def parseWebPage(html,docid,url,db):
    webpage = bs(html, 'html.parser')
    anchor.indexAnchorText(docid, url, webpage, db) # to index anchor text
    fulltext = webpage.find_all(text=True)
    tokens = []

    for element in fulltext:
        if not element.parent.name in ['style', 'script', '[document]'] \
                and not isinstance(element, bs4.element.Comment):
            tokens += tokenize(str(element))
    return filter(None, tokens)

def tokenize(text):
    tokenizer = RegexpTokenizer(r'\w+')
    tokens = tokenizer.tokenize(text)
    tokens = [str(stemmer.stem(unicode(word,errors= 'ignore').lower().strip())) for word in tokens
              if word not in stopwords and len(word) >= 3]
    return tokens

def tokenize_without_stemming(text):
    tokens = []
    punctuations_replace = '#"(){}[]<>.+,/:;=?@_|~-\n'
    punctuations_remove = '!$\'%&\\*^`'
    tokens.extend(text.translate(string.maketrans(punctuations_replace, ' ' * len(punctuations_replace)),
                                 punctuations_remove).strip().lower().split())
    return tokens

def tokenize_with_url_check(text):
    urlFormat = 'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+~]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+'
    tokenizer = RegexpTokenizer(r'\w+')
    tokens = tokenizer.tokenize(text)
    tokens = [str(stemmer.stem(unicode(word, errors='ignore').lower().strip())) for word in tokens
              if word not in stopwords and len(word) > 1 and word not in re.findall(urlFormat, word)]
    return tokens

def is_valid(url):
    if len(url) == 0:
        return False

    parsed = urlparse(url)

    try:
        return ".ics.uci.edu" in url \
            and not re.match(".*\.(css|js|bmp|gif|jpe?g|ico" + "|png|tiff?|mid|mp2|mp3|mp4"\
            + "|wav|avi|mov|mpeg|ram|m4v|mkv|ogg|ogv|pdf|txt|cpp|c|h" \
            + "|ps|eps|tex|ppt|pptx|doc|docx|xls|xlsx|names|data|dat|exe|bz2|tar|msi|bin|7z|psd|dmg|iso|epub|dll|cnf|tgz|sha1" \
            + "|thmx|mso|arff|rtf|jar|csv"\
            + "|rm|smil|wmv|swf|wma|zip|rar|gz)$", parsed.path.lower()) \
            #and is_absolute(url) \
            #and not isCrawlerTrap(url,urlObj)

    except TypeError, e:
        print ("TypeError for ", parsed)
        pass

def is_absolute(url):
    ''' Double check if the url is in absolute form '''
    try:
        return bool(urlparse(url).netloc)
    except Exception:
        return False