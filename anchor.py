import urlparse
import urllib
import re
import sys

from pymongo import MongoClient
import pymongo
reload(sys)
sys.setdefaultencoding('utf-8')
import parser
from DbInterface import DbInterface
from mapReduce import dict
from bs4 import BeautifulSoup as bs
import bs4

anchorInfo = []
anchorInfo_2gram = []
titleContent = []
titleContent2gram = []

def extractText(anchor, url):
    result = []
    if not urlparse.urlparse(url).scheme:
        url = "http://" + url
    for ele in anchor:
        if ele.has_attr('href') and not ele['href'].startswith('mailto') \
            and not ele['href'].startswith('javascript'):
            tokens = parser.tokenize_with_url_check(str(ele.text))
            result.append([' '.join(tokens), urlparse.urljoin(url, str(urllib.unquote(ele['href']))).strip('/')])
    return result

def getDocID(db, link):
    link = link.replace("http://","")
    link = link.replace("https://", "")
    # obj = db.getDocID("docIndex",link)
    # if obj is not None:
    #     return obj['docID']
    # else:
    #     return ''
    if dict.has_key(link):
        return str(dict[link])
    else:
        return ''



def insertAnchorInfo(anchorTextLink, docid, db):
    for text, link in anchorTextLink:
        text = text.split()
        if len(text) > 0 and parser.is_valid(link):
            for i in xrange(len(text)):
                destDocID = getDocID(db, link)
                if len(destDocID) > 0 and len(str(text[i])) > 1:
                    anchorInfo.append({'DB_ANCHOR_TEXT': text[i].strip(),
                                   'DB_ANCHOR_TEXT_DEST_DOCID': destDocID,
                                    'DB_ANCHOR_TEXT_SOURCE_DOCID': docid})

                    if len(text) > 1:
                        if i + 1 != len(text) and len(str(text[i + 1])) > 1:
                            anchorInfo_2gram.append({'DB_ANCHOR_TEXT': ' '.join(text[i:i + 2]),
                                                     'DB_ANCHOR_TEXT_DEST_DOCID': destDocID,
                                                     'DB_ANCHOR_TEXT_SOURCE_DOCID': docid})

    #print anchorInfo
    # if len(anchorInfo) > 0:
    #     db.insertMany('collAnchorInfo', anchorInfo)

    #print anchorInfo_2gram
    # if len(anchorInfo_2gram) > 0:
    #     db.insertMany('collAnchorInfo2gram', anchorInfo_2gram)

def indexTitle(page, docid, db):
    # print type(page.title.string)
    if page.title is not None and page.title.string is not None:
        title = page.title.string.split()
    else:
        title = ''

    for i in xrange(len(title)):
        word = ' '.join(parser.tokenize_with_url_check(str(title[i])))
        if len(word) > 1:
            titleContent.append({'DB_TITLE_NAME': word,
                             'DB_TITLE_DOCID': docid})

        if i + 1 != len(title):
            word = parser.tokenize_with_url_check(str(title[i:i + 2]))
            if len(word) > 1 and len(title[i]) > 1 and len(title[i+1]) > 1:
                word = ' '.join(word)
                titleContent2gram.append({'DB_TITLE_NAME': word,
                                 'DB_TITLE_DOCID': docid})

    # #print "title", titleContent
    # if len(titleContent) > 0:
    #     db.insertMany('collTitle', titleContent)
    #
    # #print "title2gram", titleContent2gram
    # if len(titleContent2gram) > 0:
    #     db.insertMany('collTitle2gram', titleContent2gram)

def indexAnchorText(docid, url, webpage, db):
    anchor = webpage.findAll('a')
    anchorTextLink = extractText(anchor, url)
    #print anchorTextLink
    insertAnchorInfo(anchorTextLink, docid, db)
    indexTitle(webpage, docid, db)

def getAnchorScore(db, query, anchorDict,weight):
    targetDestDoc = []
    result = db.getAnchorTargetDocID("collAnchorInfo", query)
    for ele in result:
        targetDestDoc.append(ele['DB_ANCHOR_TEXT_DEST_DOCID'])
    if len(targetDestDoc) > 0:
        score = weight / len(targetDestDoc)
        for doc in targetDestDoc:
            if anchorDict.has_key(doc):
                anchorDict[doc] += score
            else:
                anchorDict[doc] = score

def getTitleScore(db, query, titleDict, weight):
    targetDestDoc = []
    result = db.getTitleTargetDocID("collTitle", query)
    for ele in result:
        targetDestDoc.append(ele['DB_TITLE_DOCID'])
    if len(targetDestDoc) > 0:
        score = weight/ len(targetDestDoc)
        for doc in targetDestDoc:
            if titleDict.has_key(doc):
                titleDict[doc] += score
            else:
                titleDict[doc] = score

def extractAnchorScore(queryWords, db):
    anchorDict = {}
    titleDict = {}
    for i in xrange(len(queryWords)):
        # 2 grams
        if i + 1 != len(queryWords):
            newQuery = queryWords[i] + " " + queryWords[i + 1]
            getAnchorScore(db, newQuery, anchorDict,2.0)
            getTitleScore(db, newQuery, titleDict,2.0)
        #1 gram
        getAnchorScore(db, queryWords[i], anchorDict,1.0)
        getTitleScore(db, queryWords[i], titleDict,1.0)
    return anchorDict, titleDict

# f = open("./WEBPAGES_RAW/41/340")
# rawdata = f.read()
# webpage = bs(rawdata, 'html.parser')
# #url = "www.ics.uci.edu/alumni/stayconnected/stayconnected/hall_of_fame/hall_of_fame/stayconnected/stayconnected/hall_of_fame/hall_of_fame/hall_of_fame/stayconnected/hall_of_fame/hall_of_fame/hall_of_fame/stayconnected/index.php"
# url = "http://www.ics.uci.edu/~eppstein/pubs/j-no.html"
# db = DbInterface()
# indexAnchorText(1, url, webpage, db)